/* eslint-disable no-unused-expressions */
/* eslint-env mocha */

'use strict';

const assert = require('assert');
const generateFiscalCode = require('../src/fiscalcode').generateFiscalCode;

describe('fiscal code generation', function () {
  it('test generation with real fiscal code and numbers', function () {
    const name = 'davide';
    const surname = 'ceresola';
    const place = 'milano';
    const gender = 'male';
    const year = 1995;
    const month = 7;
    const day = 10;

    const fiscalCode = generateFiscalCode(surname, name, year, month, day, gender, place);
    assert.strictEqual(16, fiscalCode.length);
    assert.strictEqual('CRSDVD95L10F205B', fiscalCode);
  });

  it('test generation with real fiscal code and strings', function () {
    const name = 'davide';
    const surname = 'ceresola';
    const place = 'milano';
    const gender = 'male';
    const year = '1995';
    const month = '7';
    const day = '10';

    const fiscalCode = generateFiscalCode(surname, name, year, month, day, gender, place);
    assert.strictEqual(16, fiscalCode.length);
    assert.strictEqual('CRSDVD95L10F205B', fiscalCode);
  });

  it('test generation with real fiscal code and strings without gender', function () {
    const name = 'davide';
    const surname = 'ceresola';
    const place = 'milano';
    const gender = undefined;
    const year = '1995';
    const month = '7';
    const day = '10';

    const fiscalCode = generateFiscalCode(surname, name, year, month, day, gender, place);
    assert.strictEqual('CRSDVD95L10F205B', fiscalCode);
  });
});
