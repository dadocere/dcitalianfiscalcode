'use strict';

const birthPlaceCodes = require('../utilities/codici_comuni.json');

module.exports = function generateBirthPlaceCode (birthPlace) {
  birthPlace = birthPlace.trim().toLowerCase();
  const birthPlaceFound = birthPlaceCodes.find(function (element) {
    return element.Comune.toLowerCase() === birthPlace;
  });
  if (!birthPlaceFound) return;
  return birthPlaceFound.Codice;
};
