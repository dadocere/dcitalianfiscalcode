'use strict';

const consonants = require('../constants/consonants.constant');
const vocals = require('../constants/vocals.constant');

module.exports = function generateSurname (surname) {
  if (!surname) {
    return;
  }
  surname = surname.trim();
  const fiscalSurname = [];
  // get the first 3 consonants
  for (let i = 0; i < surname.length; i++) {
    if (consonants.indexOf(surname.charAt(i)) !== -1) {
      fiscalSurname.push(surname.charAt(i));
      if (fiscalSurname.length === 3) {
        return fiscalSurname.join('');
      }
    }
  }

  // if there arent' 3 consonants let's start to take vocals
  for (let i = 0; i < surname.length; i++) {
    if (vocals.indexOf(surname.charAt(i)) !== -1) {
      fiscalSurname.push(surname.charAt(i));
    }
    if (fiscalSurname.length === 3) {
      return fiscalSurname.join('');
    }
  }

  // if there arent' 3 letters -> add X
  for (let i = fiscalSurname.length; i < 3; i++) {
    fiscalSurname.push('X');
  }
  return fiscalSurname.join('');
};
