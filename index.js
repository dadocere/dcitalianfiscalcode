'use strict';

const fcCode = require('./src/fiscalcode');

module.exports = {
  generateFiscalCode: fcCode.generateFiscalCode,
  getFiscalCodeInfo: fcCode.getFiscalCodeInfo
};
