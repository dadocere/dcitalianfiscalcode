/* eslint-disable no-unused-expressions */
/* eslint-env mocha */

'use strict';

const assert = require('assert');
const generateBirthPlaceCode = require('../src/generators/birthPlaceCode.generator');

describe('test birthPlace code generation', function () {
  it('test birthPlace with found place', function () {
    const place = 'Albiano d\'Ivrea';
    assert.notStrictEqual(undefined, generateBirthPlaceCode(place));
    assert.strictEqual(4, (generateBirthPlaceCode(place)).length);
    assert.strictEqual('A157', (generateBirthPlaceCode(place)));
  });

  it('test birthPlace with found place', function () {
    const place = 'milano';
    assert.notStrictEqual(undefined, generateBirthPlaceCode(place));
    assert.strictEqual(4, (generateBirthPlaceCode(place)).length);
    assert.strictEqual('F205', (generateBirthPlaceCode(place)));
  });

  it('test birthPlace with no found place', function () {
    const place = 'milan';
    assert.strictEqual(undefined, generateBirthPlaceCode(place));
  });
});
