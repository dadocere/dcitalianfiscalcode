/* eslint-disable no-unused-expressions */
/* eslint-env mocha */

'use strict';

const assert = require('assert');
const generateYear = require('../src/generators/year.generator');

describe('test year generation', function () {
  it('test year with 4 characters and 0', function () {
    const year = '2004';
    assert.notStrictEqual(undefined, generateYear(year));
    assert.strictEqual(2, (generateYear(year)).length);
    assert.strictEqual('04', (generateYear(year)));
  });

  it('test year with 2 characters and 0', function () {
    const year = '04';
    assert.notStrictEqual(undefined, generateYear(year));
    assert.strictEqual(2, (generateYear(year)).length);
    assert.strictEqual('04', (generateYear(year)));
  });

  it('test year with 4 characters without 0', function () {
    const year = '2014';
    assert.notStrictEqual(undefined, generateYear(year));
    assert.strictEqual(2, (generateYear(year)).length);
    assert.strictEqual('14', (generateYear(year)));
  });

  it('test year with 2 characters without 0', function () {
    const year = '2014';
    assert.notStrictEqual(undefined, generateYear(year));
    assert.strictEqual(2, (generateYear(year)).length);
    assert.strictEqual('14', (generateYear(year)));
  });
});
