/* eslint-disable no-unused-expressions */
/* eslint-env mocha */

'use strict';

const assert = require('assert');
const generateName = require('../src/generators/name.generator');

describe('test name-code generation', function () {
  it('should return 3 characters with 3 consonant name', function () {
    const name = 'DAVIDE';
    assert.notStrictEqual(undefined, generateName(name));
    assert.strictEqual(3, (generateName(name)).length);
    assert.strictEqual('DVD', (generateName(name)));
  });

  it('should return 3 characters with 2 consonant and 1 vocals', function () {
    const name = 'DAV';
    assert.notStrictEqual(undefined, generateName(name));
    assert.strictEqual(3, (generateName(name)).length);
    assert.strictEqual('DVA', (generateName(name)));
  });

  it('should return 3 characters with 2 consonant and 1 vocals', function () {
    const name = 'DAAV';
    assert.notStrictEqual(undefined, generateName(name));
    assert.strictEqual(3, (generateName(name)).length);
    assert.strictEqual('DVA', (generateName(name)));
  });

  it('should return 3 characters with one X', function () {
    const name = 'AN';
    assert.notStrictEqual(undefined, generateName(name));
    assert.strictEqual(3, (generateName(name)).length);
    assert.strictEqual('NAX', (generateName(name)));
  });
});
