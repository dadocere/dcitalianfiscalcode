[![pipeline status](https://gitlab.com/dadocere/dcitalianfiscalcode/badges/master/pipeline.svg)](https://gitlab.com/dadocere/dcitalianfiscalcode/commits/master)[![coverage report](https://gitlab.com/dadocere/dcitalianfiscalcode/badges/master/coverage.svg)](https://gitlab.com/dadocere/dcitalianfiscalcode/commits/master)
# DCItalianFiscalCode

An Italian Fiscal Code generator available as [Node.js](https://nodejs.org/) modules.

## Installation

Using npm:
```shell
$ npm i --save dcitalianfiscalcode
```

In Node.js:
```js
// Load the module.
var fcUtility = require('dcitalianfiscalcode');
// get fiscal code passing data (gender is optional, we could pass undefined).
fcUtility.generateFiscalCode(surname, name, birthYear, birthMonth, birthDay, gender, birthPlace);
// get fiscal code info passing fiscal code.
const info = fcUtility.getFiscalCodeInfo (fiscalCode)
// access info
console.log(info.birthPlace);
console.log(info.birthProvince);
```
If gender is not passed as parameter, this module will try to detect it automatically using name

## Feature
* fiscal code generation starting from user's info
* gender auto-detection using name
* extract birth info from fiscal code (birthPlace, birthProvince)
See the [package source](https://gitlab.com/dadocere/dcitalianfiscalcode) for more details.
