'use strict';

module.exports = function generateYear (year) {
  if (!year) {
    return;
  }
  return year.trim().slice(-2);
};
