'use strict';
const genderDetection = require('gender-detection');

// day -> day if gender is male
// day -> day + 40 if gender is female
module.exports = function generateDay (day, name, gender) {
  if (!gender || ['male', 'female'].indexOf(gender.trim()) === -1) {
    gender = genderDetection.detect(name);
    console.log('Detected gender:', gender);
    if (!gender || gender === 'unknown') {
      console.log('unknown gender');
      return;
    }
  }
  gender = gender.trim();
  day = day.trim();
  if (gender === 'female') {
    if (day.length > 0 && day.charAt(0) === '0') { // remove 0 if exists
      day = day.substr(1);
    }
    return String(parseInt(day) + parseInt(40)); // add 40
  }
  return day.length === 1 ? ('0' + day) : day;
};
