'use strict';

const { oddConstants, evenConstants, finalLetterConstants } = require('../constants/letterCheck.constants');

module.exports = function generateLetterCheck (input) {
  let sum = 0;
  for (let i = 0; i < input.length; i++) {
    const currentChar = input.charAt(i);
    if ((i + 1) % 2 === 0) {
      sum = sum + evenConstants[currentChar];
    } else {
      sum = sum + oddConstants[currentChar];
    }
  }
  return finalLetterConstants[sum % 26];
};
