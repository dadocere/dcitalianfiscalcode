'use strict';

const monthMapping = require('../constants/monthMapping.constant');

module.exports = function generateMonth (month) {
  if (!month) {
    return;
  }
  month = month.trim();
  if (month.length === 1) {
    month = '0' + month;
  }
  return monthMapping[month];
};
