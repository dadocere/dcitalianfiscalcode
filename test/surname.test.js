/* eslint-disable no-unused-expressions */
/* eslint-env mocha */

'use strict';

const assert = require('assert');
const generateSurname = require('../src/generators/surname.generator');

describe('test surname-code generation', function () {
  it('should return 3 characters with 3 consonant surname', function () {
    const surname = 'CERESOLA';
    assert.notStrictEqual(undefined, generateSurname(surname));
    assert.strictEqual(3, (generateSurname(surname)).length);
    assert.strictEqual('CRS', (generateSurname(surname)));
  });

  it('should return 3 characters with 2 consonant and 1 vocals', function () {
    const surname = 'CARA';
    assert.notStrictEqual(undefined, generateSurname(surname));
    assert.strictEqual(3, (generateSurname(surname)).length);
    assert.strictEqual('CRA', (generateSurname(surname)));
  });

  it('should return 3 characters with 2 consonant and 1 vocals', function () {
    const surname = 'CAAR';
    assert.notStrictEqual(undefined, generateSurname(surname));
    assert.strictEqual(3, (generateSurname(surname)).length);
    assert.strictEqual('CRA', (generateSurname(surname)));
  });

  it('should return 3 characters with one X', function () {
    const surname = 'CA';
    assert.notStrictEqual(undefined, generateSurname(surname));
    assert.strictEqual(3, (generateSurname(surname)).length);
    assert.strictEqual('CAX', (generateSurname(surname)));
  });

  it('should return 3 characters with a real fiscal code', function () {
    const surname = 'CERESOLA';
    assert.notStrictEqual(undefined, generateSurname(surname));
    assert.strictEqual(3, (generateSurname(surname)).length);
    assert.strictEqual('CRS', (generateSurname(surname)));
  });
});
