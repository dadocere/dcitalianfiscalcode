'use strict';

const generateBirthPlaceCode = require('./generators/birthPlaceCode.generator');
const generateDay = require('./generators/day.generator');
const generateLetterCheck = require('./generators/letterCheck.generator');
const generateMonth = require('./generators/month.generator');
const generateName = require('./generators/name.generator');
const generateSurname = require('./generators/surname.generator');
const generateYear = require('./generators/year.generator');

const extractBirthPlate = require('./extractors/birthPlace.extractor');

function generateFiscalCode (surname, name, year, month, day, gender, birthPlace) {
  if (!surname || !name || !year || !month || !day || !birthPlace) {
    return;
  }
  const genSurname = generateSurname(surname.toUpperCase());
  const genName = generateName(name.toUpperCase());
  const genYear = generateYear(year.toString().toUpperCase());
  const genMonth = generateMonth(month.toString().toUpperCase());
  const genBirthPlaceCode = generateBirthPlaceCode(birthPlace.toUpperCase());
  if (gender) gender = gender.toLowerCase();
  const genDay = generateDay(day.toString().toUpperCase(), name, gender);

  if (!genSurname || !genName || !genYear || !genMonth || !genDay || !genBirthPlaceCode) {
    return;
  }
  const partialFC = genSurname + genName + genYear + genMonth + genDay + genBirthPlaceCode;
  const controlCheck = generateLetterCheck(partialFC);

  return partialFC + controlCheck;
}

function getFiscalCodeInfo (fiscalCode) {
  if (fiscalCode.length !== 16) {
    return;
  }
  return extractBirthPlate(fiscalCode);
}

module.exports = {
  generateFiscalCode: generateFiscalCode,
  getFiscalCodeInfo: getFiscalCodeInfo
};
