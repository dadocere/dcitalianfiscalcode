/* eslint-disable no-unused-expressions */
/* eslint-env mocha */

'use strict';

const assert = require('assert');
const getFiscalCodeInfo = require('../src/fiscalcode').getFiscalCodeInfo;

describe('fiscal code extractor', function () {
  it('test generation with real fiscal code', function () {
    const fiscalCode = 'CRSDVD95L10F205B';

    const fcInfos = getFiscalCodeInfo(fiscalCode);
    assert.strictEqual('Milano', fcInfos.birthPlace);
    assert.strictEqual('MI', fcInfos.birthProvince);
  });

  it('test generation with NON real fiscal code', function () {
    const fiscalCode = 'CRSDVD95L10F204B';

    const fcInfos = getFiscalCodeInfo(fiscalCode);
    assert.strictEqual(undefined, fcInfos);
  });
});
