/* eslint-disable no-unused-expressions */
/* eslint-env mocha */

'use strict';

const assert = require('assert');
const extractBirthPlate = require('../src/extractors/birthPlace.extractor');

describe('birth place extractor', function () {
  it('test with real fc', function () {
    const fiscalCode = 'CRSDVD95L10F205B';
    assert.notStrictEqual(undefined, extractBirthPlate(fiscalCode));
    assert.strictEqual('Milano', (extractBirthPlate(fiscalCode).birthPlace));
    assert.strictEqual('MI', (extractBirthPlate(fiscalCode).birthProvince));
  });

  it('test with non real fc', function () {
    const fiscalCode = 'CRSDVD95L10F204B';
    assert.strictEqual(undefined, extractBirthPlate(fiscalCode));
  });
});
