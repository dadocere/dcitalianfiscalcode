'use strict';

const birthPlaceCodes = require('../utilities/codici_comuni.json');
const birthPlaceInfo = require('../utilities/comuni_province.json');

module.exports = function extractBirthPlate (fiscalCode) {
  const placeCode = fiscalCode.substring(11, 15).toUpperCase();
  const birthPlaceFound = birthPlaceCodes.find(function (element) {
    return element.Codice === placeCode;
  });
  const provinceFound = birthPlaceInfo.find(function (element) {
    return element.CodFisco === placeCode;
  });

  if (!birthPlaceFound || !provinceFound) {
    return;
  }

  return { birthPlace: birthPlaceFound.Comune, birthProvince: provinceFound.Provincia };
};
