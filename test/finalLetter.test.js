/* eslint-disable no-unused-expressions */
/* eslint-env mocha */

'use strict';

const assert = require('assert');
const generateLetterCheck = require('../src/generators/letterCheck.generator');

describe('letter check generation', function () {
  it('test letter check with real fiscal code', function () {
    const fc = 'CRSDVD95L10F205';
    assert.notStrictEqual(undefined, generateLetterCheck(fc));
    assert.strictEqual(1, (generateLetterCheck(fc)).length);
    assert.strictEqual('B', (generateLetterCheck(fc)));
  });

  it('test letter check with real fiscal code', function () {
    const fc = 'RVZVNT87H66A794';
    assert.notStrictEqual(undefined, generateLetterCheck(fc));
    assert.strictEqual(1, (generateLetterCheck(fc)).length);
    assert.strictEqual('D', (generateLetterCheck(fc)));
  });

  it('test letter check with real fiscal code', function () {
    const fc = 'GNGGTN82B13A662';
    assert.notStrictEqual(undefined, generateLetterCheck(fc));
    assert.strictEqual(1, (generateLetterCheck(fc)).length);
    assert.strictEqual('B', (generateLetterCheck(fc)));
  });

  it('test letter check with real fiscal code', function () {
    const fc = 'LBNLNR94P48A662';
    assert.notStrictEqual(undefined, generateLetterCheck(fc));
    assert.strictEqual(1, (generateLetterCheck(fc)).length);
    assert.strictEqual('U', (generateLetterCheck(fc)));
  });
});
