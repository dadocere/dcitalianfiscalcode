'use strict';

const consonants = require('../constants/consonants.constant');
const vocals = require('../constants/vocals.constant');

module.exports = function generateName (name) {
  if (!name) {
    return;
  }
  name = name.trim();
  let fiscalName = [];
  // get the first 3 consonants skipping the second one
  let consonantsFound = 0;
  for (let i = 0; i < name.length; i++) {
    if (consonants.indexOf(name.charAt(i)) !== -1) {
      consonantsFound++;
      if (consonantsFound === 2) {
        continue;
      }
      fiscalName.push(name.charAt(i));
      if (fiscalName.length === 3) {
        return fiscalName.join('');
      }
    }
  }

  // if comes here we need to take the first 3 consonants
  fiscalName = [];
  for (let i = 0; i < name.length; i++) {
    if (consonants.indexOf(name.charAt(i)) !== -1) {
      fiscalName.push(name.charAt(i));
      if (fiscalName.length === 3) {
        return fiscalName.join('');
      }
    }
  }

  // if there arent' 3 consonants let's start to take vocals
  for (let i = 0; i < name.length; i++) {
    if (vocals.indexOf(name.charAt(i)) !== -1) {
      fiscalName.push(name.charAt(i));
    }
    if (fiscalName.length === 3) {
      return fiscalName.join('');
    }
  }

  // here we need to add some X
  for (let i = fiscalName.length; i < 3; i++) {
    fiscalName.push('X');
  }
  return fiscalName.join('');
};
