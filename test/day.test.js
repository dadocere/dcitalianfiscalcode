/* eslint-disable no-unused-expressions */
/* eslint-env mocha */

'use strict';

const assert = require('assert');
const generateDay = require('../src/generators/day.generator');

describe('test day generation', function () {
  it('test day with 2 characters with 0 and male', function () {
    const day = '04';
    const name = '';
    assert.notStrictEqual(undefined, generateDay(day, name, 'male'));
    assert.strictEqual(2, (generateDay(day, name, 'male')).length);
    assert.strictEqual('04', (generateDay(day, name, 'male')));
  });

  it('test day with 2 characters with 0 and female', function () {
    const day = '04';
    const name = '';
    assert.notStrictEqual(undefined, generateDay(day, name, 'female'));
    assert.strictEqual(2, (generateDay(day, name, 'female')).length);
    assert.strictEqual('44', (generateDay(day, name, 'female')));
  });

  it('test day with 2 characters without 0 and male', function () {
    const day = '14';
    const name = '';
    assert.notStrictEqual(undefined, generateDay(day, name, 'male'));
    assert.strictEqual(2, (generateDay(day, name, 'male')).length);
    assert.strictEqual('14', (generateDay(day, name, 'male')));
  });

  it('test day with 2 characters without 0 and female', function () {
    const day = '14';
    const name = '';
    assert.notStrictEqual(undefined, generateDay(day, name, 'female'));
    assert.strictEqual(2, (generateDay(day, name, 'female')).length);
    assert.strictEqual('54', (generateDay(day, name, 'female')));
  });

  it('test day with 1 character and male', function () {
    const day = '4';
    const name = '';
    assert.notStrictEqual(undefined, generateDay(day, name, 'male'));
    assert.strictEqual(2, (generateDay(day, name, 'male')).length);
    assert.strictEqual('04', (generateDay(day, name, 'male')));
  });

  it('test day with 1 character1 and female', function () {
    const day = '4';
    const name = '';
    assert.notStrictEqual(undefined, generateDay(day, name, 'female'));
    assert.strictEqual(2, (generateDay(day, name, 'female')).length);
    assert.strictEqual('44', (generateDay(day, name, 'female')));
  });

  it('test day with 2 characters without 0 and  gender detection system', function () {
    const day = '14';
    const name = 'Davide';
    assert.notStrictEqual(undefined, generateDay(day, name, undefined));
    assert.strictEqual(2, (generateDay(day, name, undefined)).length);
    assert.strictEqual('14', (generateDay(day, name, undefined)));
  });
});
