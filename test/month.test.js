/* eslint-disable no-unused-expressions */
/* eslint-env mocha */

'use strict';

const assert = require('assert');
const generateMonth = require('../src/generators/month.generator');

describe('test month generation', function () {
  it('test month with 2 characters and 0', function () {
    const month = '04';
    assert.notStrictEqual(undefined, generateMonth(month));
    assert.strictEqual(1, (generateMonth(month)).length);
    assert.strictEqual('D', (generateMonth(month)));
  });

  it('test month with 1 character without 0', function () {
    const month = '4';
    assert.notStrictEqual(undefined, generateMonth(month));
    assert.strictEqual(1, (generateMonth(month)).length);
    assert.strictEqual('D', (generateMonth(month)));
  });

  it('test month with 2 characters with 0', function () {
    const month = '10';
    assert.notStrictEqual(undefined, generateMonth(month));
    assert.strictEqual(1, (generateMonth(month)).length);
    assert.strictEqual('R', (generateMonth(month)));
  });

  it('test month with 2 characters without 0', function () {
    const month = '11';
    assert.notStrictEqual(undefined, generateMonth(month));
    assert.strictEqual(1, (generateMonth(month)).length);
    assert.strictEqual('S', (generateMonth(month)));
  });
});
